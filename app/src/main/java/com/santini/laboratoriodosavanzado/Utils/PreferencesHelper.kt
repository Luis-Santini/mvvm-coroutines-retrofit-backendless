package com.santini.laboratoriodosavanzado.Utils

import android.content.Context
import android.content.SharedPreferences

object PreferencesHelper {
    private val PREFERENCES = "com.santini.laboratoriodosavanzado"
    private val PREFERENCES_USERNAME = "$PREFERENCES.username"
    private val PREFERENCES_TOKEN = "$PREFERENCES.token"


    fun saveSession(context: Context, username: String, token:String) {
        val editor = getEditor(context)
        editor.putString(PREFERENCES_USERNAME, username)
        editor.putString(PREFERENCES_TOKEN, token)
        editor.apply()
    }

    fun session(context: Context): String? {
        val sharedPreferences = getSharedPreferences(context)
        return sharedPreferences.getString(PREFERENCES_TOKEN, null)
    }

    private fun getEditor(context: Context): SharedPreferences.Editor {
        val preferences = getSharedPreferences(context)
        return preferences.edit()
    }

    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE)
    }
}