package com.santini.laboratoriodosavanzado.data

import com.santini.laboratoriodosavanzado.data.remote.LoginDataSource
import com.santini.laboratoriodosavanzado.data.LoginResult
import com.santini.laboratoriodosavanzado.model.UserModel

class LoginRepository(private val loginDataSource: LoginDataSource) {

    suspend fun RequestLoginRepository(userName: String, password: String): LoginResult<UserModel> {
        return loginDataSource.loginDS(userName, password)
    }


}
