package com.santini.laboratoriodosavanzado.data

import android.os.RemoteException


sealed class LoginResult<out T> {
    data class Success<T>(val data: T) : LoginResult<T>()
    data class SuccessList<T>(val data: List<T>) : LoginResult<T>()
    data class Failure(val exception: RemoteException) : LoginResult<Nothing>()

}
