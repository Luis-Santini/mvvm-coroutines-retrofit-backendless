package com.santini.laboratoriodosavanzado.data.remote

import com.santini.laboratoriodosavanzado.model.AllContact
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

object ApiClient {
    private const val API_BASE_URL = "https://api.backendless.com/"
    private val okHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor(interceptor())
            .build()
    }
    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
    }

    fun build(): NetWorkingService {
        return retrofit.create(NetWorkingService::class.java)
    }

    private fun interceptor(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        return httpLoggingInterceptor
    }

    fun getInstance() = this


    interface NetWorkingService {
        @POST("/{applicationId}/{restApiKey}/users/login")
        suspend fun loginApi(
            @Path("applicationId") appID: String,
            @Path("restApiKey") restApiKey: String, @Body logInRaw: LoginRequest
        ): Response<LoginResponse>


        @GET("/{applicationid}/{restapikey}/data/agenda")
        suspend fun allcontactApi(
            @Path("applicationid") appID: String,
            @Path("restapikey") restApiKEY: String,
            @HeaderMap headers: Map<String, String>?
        ): Response<List<AllContact>>

    }


}