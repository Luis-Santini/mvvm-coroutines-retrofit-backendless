package com.santini.laboratoriodosavanzado.data.remote

import com.santini.laboratoriodosavanzado.data.LoginResult
import com.santini.laboratoriodosavanzado.model.UserModel

class AuthRepository(private val dataSource: LoginDataSource) {
    suspend fun loginAR(
        userName: String,
        password: String
    ): LoginResult<UserModel> {
        return dataSource.loginDS(userName, password)
    }
}