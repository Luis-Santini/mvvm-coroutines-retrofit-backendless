package com.santini.laboratoriodosavanzado.data.remote


class ContacListRepository(private val dataSource: ContactDataSourse) {
    suspend fun fetch(token:String) = dataSource.getAllContacs(token)
}