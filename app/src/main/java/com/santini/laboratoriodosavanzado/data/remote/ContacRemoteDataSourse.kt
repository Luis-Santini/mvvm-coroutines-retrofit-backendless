package com.santini.laboratoriodosavanzado.data.remote

import android.os.RemoteException
import com.santini.laboratoriodosavanzado.data.LoginResult
import com.santini.laboratoriodosavanzado.model.AllContact
import java.io.IOException

class ContacRemoteDataSourse (apiClient :ApiClient):ContactDataSourse {

    private val service = apiClient.build()
    override suspend fun getAllContacs(token: String): LoginResult<AllContact> {

        return try {
            val headerMap = mutableMapOf<String,String>()
            headerMap["user-token"] = token
            val response = service.allcontactApi(
                Constants.APPLICATION_ID,
                Constants.REST_API_KEY,
                headerMap
            )
            if (response.isSuccessful){
                response?.body()?.let {
                    val data:List<AllContact> = it
                    LoginResult.SuccessList(data)
                }?:run {
                    LoginResult.Failure(RemoteException(ERROR_MESSAGE))
                }
            }else{
                LoginResult.Failure(RemoteException(ERROR_MESSAGE))
            }
        }catch(e: IOException){
            LoginResult.Failure(
                RemoteException(ERROR_MESSAGE)
            )
        }
    }
    companion object {
        private const val ERROR_MESSAGE: String = "Ocurrió un error"
    }

}