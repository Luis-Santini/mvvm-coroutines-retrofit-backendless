package com.santini.laboratoriodosavanzado.data.remote

import com.santini.laboratoriodosavanzado.data.LoginResult
import com.santini.laboratoriodosavanzado.model.AllContact

interface ContactDataSourse {
    suspend fun getAllContacs(token: String):LoginResult<AllContact>
}