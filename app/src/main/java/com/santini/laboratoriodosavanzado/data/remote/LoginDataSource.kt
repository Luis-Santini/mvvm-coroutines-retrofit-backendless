package com.santini.laboratoriodosavanzado.data.remote

import com.santini.laboratoriodosavanzado.data.LoginResult
import com.santini.laboratoriodosavanzado.model.UserModel

interface LoginDataSource {

    suspend fun loginDS(userName: String, password: String): LoginResult<UserModel>
    fun cancel()
}