package com.santini.laboratoriodosavanzado.data.remote

import android.os.RemoteException
import android.util.Log
import com.santini.laboratoriodosavanzado.data.LoginResult
import com.santini.laboratoriodosavanzado.model.UserModel
import java.io.IOException


class LoginRemoteDataSource(apiClient: ApiClient) :
    LoginDataSource {

    private val service = apiClient.build()
    override suspend fun loginDS(userName: String, password: String): LoginResult<UserModel> {
        val loginRaw = LoginRequest(userName, password)
        //val AplicationID = Constants.APPLICATION_ID
        //      val apikey = Constants.REST_API_KEY
        return try {
            val response =
                service.loginApi(Constants.APPLICATION_ID, Constants.REST_API_KEY, loginRaw)
            //  val response = service.loginApi(AplicationID, apikey,loginRaw)

            if (response.isSuccessful) {
                response?.body()?.let { it ->
                    val user = UserModel(
                        it.objectId,
                        it.token, it.email, it.name
                    )
                    LoginResult.Success(user)
                } ?: run {
                    LoginResult.Failure(RemoteException(ERROR_MESSAGE))
                }
            } else {
                LoginResult.Failure(RemoteException(ERROR_MESSAGE))
            }

        } catch (e: IOException) {
            LoginResult.Failure(
                RemoteException(ERROR_MESSAGE)
            )
        }
    }

    override fun cancel() {}

    companion object {
        private const val ERROR_MESSAGE: String = "Ocurrió un error"
    }

}