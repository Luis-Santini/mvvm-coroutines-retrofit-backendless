package com.santini.laboratoriodosavanzado.data.remote

data class LoginRequest(val login: String?, val password: String?)
