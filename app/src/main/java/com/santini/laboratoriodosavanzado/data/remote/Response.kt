package com.santini.laboratoriodosavanzado.data.remote

import android.provider.ContactsContract
import com.google.gson.annotations.SerializedName

data class LoginResponse(
    @SerializedName("user-token") val token: String?,
    val email: String?,
    val name: String?, val objectId: String?
)
