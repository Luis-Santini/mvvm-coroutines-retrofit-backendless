package com.santini.laboratoriodosavanzado.model

import java.io.Serializable

data class AllContact(val objectId:String?, val phone:String?, val name:String?,
                      val ownerId:String?): Serializable