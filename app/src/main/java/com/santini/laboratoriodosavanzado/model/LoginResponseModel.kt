package com.santini.laboratoriodosavanzado.model

import java.io.Serializable

data class UserModel(
    val id: String?, val token: String?, val email: String?,
    val name: String?
) : Serializable