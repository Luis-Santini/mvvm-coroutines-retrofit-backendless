package com.santini.laboratoriodosavanzado.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.santini.laboratoriodosavanzado.R
import com.santini.laboratoriodosavanzado.Utils.PreferencesHelper
import com.santini.laboratoriodosavanzado.data.LoginResult
import kotlinx.android.synthetic.main.activity_add_new_contact.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

private var name:String?= null
private var numberphone:String?= null
class AddNewContactActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_contact)

        ui()
    }

    private fun ui(){
        btnAddNewContact.setOnClickListener {
                addNewContact()
        }
    }
    private fun  addNewContact(){
        name = edtname.text.toString()
        numberphone = edtPhone.text.trim().toString()

        val token = PreferencesHelper.session(this)?:""
     /*   lifecycleScope.launch {
            val result = withContext(Dispatchers.IO){
                repository.fetch(token)
            }
            //   hideLoading()
            when(result){
                is LoginResult.SuccessList ->{
                    renderContacts(result.data)
                }

                is LoginResult.Failure ->    {
                    showErrorMessage(result.exception.message?:"Ocurrió un error")
                }
            }
        }*/

    }
}