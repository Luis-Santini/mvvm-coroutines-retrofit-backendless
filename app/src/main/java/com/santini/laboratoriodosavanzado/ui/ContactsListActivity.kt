package com.santini.laboratoriodosavanzado.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Adapter
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.santini.laboratoriodosavanzado.R
import com.santini.laboratoriodosavanzado.Utils.PreferencesHelper
import com.santini.laboratoriodosavanzado.data.LoginResult
import com.santini.laboratoriodosavanzado.data.remote.ApiClient
import com.santini.laboratoriodosavanzado.data.remote.ContacListRepository
import com.santini.laboratoriodosavanzado.data.remote.ContacRemoteDataSourse
import com.santini.laboratoriodosavanzado.model.AllContact
import com.santini.laboratoriodosavanzado.ui.adapter.ContacAdapter
import kotlinx.android.synthetic.main.activity_contacts_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ContactsListActivity : AppCompatActivity() {

    private lateinit var adapter: ContacAdapter

    private val repository : ContacListRepository by lazy {
        ContacListRepository(ContacRemoteDataSourse(ApiClient.getInstance()))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contacts_list)

        uiContact()
        loadContact()
    }

    private fun uiContact(){
        rvContact.layoutManager = LinearLayoutManager(this)
        adapter = ContacAdapter(emptyList()) { itContact->
            showMessage("item ${itContact.name}")
          //  goToNote(itNote)
        }

        rvContact.adapter = adapter

       fbNewContact.setOnClickListener {
            AddNewContact()
        }
    }


    private fun loadContact(){
       // showLoading()
        val token = PreferencesHelper.session(this)?:""
        lifecycleScope.launch {
            val result = withContext(Dispatchers.IO){
                repository.fetch(token)
            }
         //   hideLoading()
            when(result){
                is LoginResult.SuccessList ->{
             //       places.sortedWith(compareBy(String.CASE_INSENSITIVE_ORDER, { result.data }))

                    renderContacts (result.data)
                }

                is LoginResult.Failure ->    {
                    showErrorMessage(result.exception.message?:"Ocurrió un error")
                }
            }
        }
    }

    private fun AddNewContact(){
        startActivity(Intent(this, AddNewContactActivity::class.java))
    }

    private fun renderContacts(contact:List<AllContact>){
       var resultado = contact.sortedWith(compareBy ({it.name}))
        adapter.update(resultado)
    }
    private fun showMessage(message:String){
        Toast.makeText(this,"item $message",Toast.LENGTH_SHORT).show()
    }

    private fun showErrorMessage(error: String?) {
        Toast.makeText(this, "Error : $error", Toast.LENGTH_SHORT).show()
    }

}