package com.santini.laboratoriodosavanzado.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.santini.laboratoriodosavanzado.R
import com.santini.laboratoriodosavanzado.Utils.PreferencesHelper
import com.santini.laboratoriodosavanzado.data.LoginRepository
import com.santini.laboratoriodosavanzado.data.LoginResult
import com.santini.laboratoriodosavanzado.data.remote.ApiClient
import com.santini.laboratoriodosavanzado.data.remote.AuthRepository
import com.santini.laboratoriodosavanzado.data.remote.LoginRemoteDataSource
import com.santini.laboratoriodosavanzado.model.UserModel
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginActivity : AppCompatActivity() {

    /* private lateinit var viewModel: LoginViewModel  // creo una instancia del view model (cada activity debe tener su ViewModel)

     private var username:String? = null
     private var password: String? = null
     override fun onCreate(savedInstanceState: Bundle?) {
         super.onCreate(savedInstanceState)
         setContentView(R.layout.activity_login)

         btnLogin.setOnClickListener{
             Login()
         }
     }

     private fun Login() {

         viewModel.RequestLoginViewModel(username.toString(), password.toString())

     }*/
    private val repository by lazy {
        AuthRepository(
            LoginRemoteDataSource(
                ApiClient.getInstance()
            )
        )
    }

    /* private fun showViewLoading() {
         flayLoading.visibility = View.VISIBLE
     }

     private fun hideViewLoading() {
         flayLoading.visibility = View.GONE
     }*/

    private fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun validateForm(): Boolean {
        val email = edtEmail.text.trim()
        val password = edtPassword.text.trim()

        if (email.isEmpty()) {
            return false
        }

        if (password.isEmpty()) {
            return false
        }
        return true
    }

    private fun setupUi() {
        btnLogin.setOnClickListener {
            if (validateForm()) {
                Login()
            }
        }
    }

    private fun operationCompleted(user: UserModel?) {
        Log.v("CONSOLE", "user $user")
        showMessage("Usuario Logueado")
        startActivity(Intent(this,ContactsListActivity::class.java))
    }

    private fun saveSession(user: UserModel){
         val email = user.email?:""
         val token = user.token?:""
         PreferencesHelper.saveSession(this,email,
             token)
     }

    private fun Login() {
        // showViewLoading()
        val email = edtEmail.text.trim().toString()
        val password = edtPassword.text.trim().toString()
        lifecycleScope.launch {
            val result = withContext(Dispatchers.IO) {
                repository.loginAR(email, password)
            }
            //  hideViewLoading()
            when (result) {
                is LoginResult.Success -> {
                    val user = result.data
                    saveSession(user)
                    operationCompleted(user)
                }

                is LoginResult.Failure -> {
                    showMessage(result.exception.message ?: "Ocurrió un error")
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupUi()
    }
}