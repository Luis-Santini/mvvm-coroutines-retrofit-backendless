package com.santini.laboratoriodosavanzado.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.santini.laboratoriodosavanzado.R
import com.santini.laboratoriodosavanzado.model.AllContact
import kotlinx.android.synthetic.main.contact_row.view.*

class ContacAdapter(
    private var contacts: List<AllContact>,
    private val itemCallback: (contact: AllContact) -> Unit?
) : RecyclerView.Adapter<ContacAdapter.ContacViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContacAdapter.ContacViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.contact_row,
            parent,
            false
        )
        return ContacViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContacAdapter.ContacViewHolder, position: Int) {
        holder.bind(contacts[position])
        holder.view.setOnClickListener {
            itemCallback(contacts[position])
        }
    }

    override fun getItemCount(): Int {
       return contacts.size
    }

    fun update(data:List<AllContact>){
        contacts= data
        notifyDataSetChanged()
        //DiffUtils
    }
    class ContacViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(contact: AllContact) {
            view.txtName.text = contact.name
            view.txtNumberPhone.text = contact.phone
        }
    }
}
