package com.santini.laboratoriodosavanzado.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.santini.laboratoriodosavanzado.data.remote.LoginResponse
import com.santini.laboratoriodosavanzado.data.LoginResult
import com.santini.laboratoriodosavanzado.data.LoginRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class LoginViewModel(private val repository: LoginRepository) : ViewModel() {

    private val _isViewLoading = MutableLiveData<Boolean>()
    val isViewLoading: LiveData<Boolean> = _isViewLoading

    private val _onMessageError = MutableLiveData<Any>()
    val onMessageError: LiveData<Any> = _onMessageError

    private val _user = MutableLiveData<LoginResponse>()
    val user: LiveData<LoginResponse> = _user

    private fun validateForm(email: String, password: String): Boolean {
        if (email.isEmpty()) {
            _onMessageError.value = "Email inválido"
            return false
        }

        if (password.isEmpty()) {
            _onMessageError.value = "Password inválido"
            return false
        }
        return true
    }


    fun RequestLoginViewModel(userName: String, password: String) {

        if (validateForm(userName, password)) {
            _isViewLoading.value = true

            viewModelScope.launch {

                val result = withContext(Dispatchers.IO) {
                    repository.RequestLoginRepository(userName, password)

                }



                _isViewLoading.value = false
                when (result) {
                    is LoginResult.Success -> {
                        //    _user.value = result.data
                    }

                    is LoginResult.Failure -> {
                        _onMessageError.value = result.exception.message ?: "Ocurrió un error"
                    }
                }

            }


        }

    }


}